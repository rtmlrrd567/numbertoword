﻿
using System;

namespace Project1_Lorredo_Urizza_Bosito_Alanguilan_BSIT1C
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.Write("Enter Number: ");
            string Input = Console.ReadLine();
            Input = Words(Convert.ToInt32(Input));
            Console.Write("In words: {0}", Input);
            Console.ReadKey(true);
        }
        static string Words(int Number)
        {
            string command = "";
            string[] Units = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
            string[] Tens = { "", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

            if (Number == 0)
            {
                return "Zero";
            }
            else if (Number < 0)
            {
                return "Out of Range";
            }
            else if (Number > 1000000)
            {
                return "Out of Range";
            }

            if ((Number / 1000000) > 0)
            {
                command += Words(Number / 1000000) + " Million ";
                Number %= 1000000;
            }

            if ((Number / 1000) > 0)
            {
                command += Words(Number / 1000) + " Thousand ";
                Number %= 1000;
            }

            if ((Number / 100) > 0)
            {
                command += Words(Number / 100) + " Hundred ";
                Number %= 100;
            }

            if (Number > 0)
            {
                if (Number < 20)
                    command += Units[Number];
                else
                {
                    command += Tens[Number / 10];
                    if ((Number % 10) > 0)
                        command += " " + Units[Number % 10];
                }
            }
            return command;
		}
	}
}